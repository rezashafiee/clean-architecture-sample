package com.example.data.models


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RandomImageDto(
    @SerializedName("message")
    @Expose
    val message: String?,
    @SerializedName("status")
    @Expose
    val status: String?
)