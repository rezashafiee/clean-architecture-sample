package com.example.data.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitFactory {

    fun create() : Retrofit {
        return Retrofit.Builder().run {
            baseUrl("https://dog.ceo/api/")
            addConverterFactory(GsonConverterFactory.create())
            build()
        }
    }

}