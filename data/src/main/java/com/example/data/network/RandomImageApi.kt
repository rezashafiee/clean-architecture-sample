package com.example.data.network

import com.example.data.models.RandomImageDto
import retrofit2.Response
import retrofit2.http.GET

interface RandomImageApi {

    @GET("breeds/image/random")
    suspend fun getARandomImage(): Response<RandomImageDto>
}