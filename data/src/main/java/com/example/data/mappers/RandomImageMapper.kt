package com.example.data.mappers

import com.example.data.models.RandomImageDto
import de.troido.reza.domain.Mapper
import de.troido.reza.domain.random_image.models.RandomImage

class RandomImageMapper: Mapper<RandomImageDto, RandomImage> {
    override fun map(from: RandomImageDto): RandomImage {
        return RandomImage(
            from.message
        )
    }
}