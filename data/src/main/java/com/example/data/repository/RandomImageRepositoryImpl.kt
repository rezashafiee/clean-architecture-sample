package com.example.data.repository

import com.example.data.mappers.RandomImageMapper
import com.example.data.models.RandomImageDto
import com.example.data.network.RandomImageApi
import de.troido.reza.domain.random_image.models.RandomImage
import de.troido.reza.domain.random_image.repository.RandomImageRepository

class RandomImageRepositoryImpl(
    private val randomImageApi: RandomImageApi,
    private val randomImageMapper: RandomImageMapper
) : RandomImageRepository {

    override suspend fun getARandomImage(): RandomImage {

        return try {
            val response = randomImageApi.getARandomImage()
            randomImageMapper.map(response.body() ?: throw Exception("Empty response"))
        } catch (e: Exception) {
            e.printStackTrace()
            RandomImage("")
        }
    }
}