package com.example.myapplication.view_matchers

import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.matcher.BoundedMatcher
import org.hamcrest.Description
import org.hamcrest.Matcher

fun withItemCount(expectedValue: Int): Matcher<View> =
    object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {

        override fun describeTo(description: Description?) {
            description?.appendText("with item count : $expectedValue")
        }

        override fun matchesSafely(recyclerView: RecyclerView?): Boolean =
            recyclerView?.adapter?.itemCount == expectedValue
    }

fun withGridLayoutColumnCount(expectedValue: Int): Matcher<View> =
    object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {

        override fun describeTo(description: Description?) {
            description?.appendText("with GridLayout column count : $expectedValue")
        }

        override fun matchesSafely(recyclerView: RecyclerView?): Boolean =
            (recyclerView?.layoutManager as GridLayoutManager).spanCount == expectedValue
    }