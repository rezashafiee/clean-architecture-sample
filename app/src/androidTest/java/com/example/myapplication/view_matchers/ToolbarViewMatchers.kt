package com.example.myapplication.view_matchers

import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.ActionMenuView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.children
import androidx.core.view.forEach
import androidx.test.espresso.matcher.BoundedMatcher
import org.hamcrest.Description
import org.hamcrest.Matcher

fun withTitle(expectedValue: CharSequence): Matcher<View> =
    object : BoundedMatcher<View, Toolbar>(Toolbar::class.java) {
        override fun describeTo(description: Description?) {
            description?.appendText("With title : $expectedValue")
        }

        override fun matchesSafely(toolBar: Toolbar?): Boolean {

            toolBar?.children?.forEach {
                if (it is TextView) {
                    if (it.text.equals(expectedValue))
                        return true
                }
            }
            return false
        }
    }

fun withActionMenuTitle(expectedValue: CharSequence): Matcher<View> =
    object : BoundedMatcher<View, Toolbar>(Toolbar::class.java) {
        override fun describeTo(description: Description?) {
            description?.appendText("With title : $expectedValue")
        }

        override fun matchesSafely(toolBar: Toolbar?): Boolean {

            toolBar?.children?.forEach {
                if (it is ActionMenuView) {
                    it.menu.forEach { item ->
                        if (item.title == expectedValue)
                            return true
                    }
                }
            }
            return false
        }
    }