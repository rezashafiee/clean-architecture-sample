package com.example.myapplication.test_page

import android.app.Activity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.myapplication.R
import junit.framework.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class TestActivityTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(TestActivity::class.java)

    @Test
    fun testVisibility_activity() {
        onView(withId(R.id.root_testActivity)).check(matches(isDisplayed()))
    }

    @Test
    fun testVisibility_backButton() {
        onView(withId(R.id.btn_back)).check(matches(isDisplayed()))
    }

    @Test
    fun testTitle_backButton() {
        onView(withId(R.id.btn_back)).check(matches(withText("Back")))
    }

    @Test
    fun testAction_backButton() {
        onView(withId(R.id.btn_back)).perform(click())
        assertTrue(activityRule.scenario.result.resultCode == Activity.RESULT_CANCELED)
    }
}