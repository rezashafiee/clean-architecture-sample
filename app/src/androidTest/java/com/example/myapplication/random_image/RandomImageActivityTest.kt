package com.example.myapplication.random_image

import android.content.pm.ActivityInfo
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.myapplication.R
import com.example.myapplication.adapters.RandomImageAdapter
import com.example.myapplication.rules.EspressoIdlingResourceRule
import com.example.myapplication.view_matchers.withActionMenuTitle
import com.example.myapplication.view_matchers.withGridLayoutColumnCount
import com.example.myapplication.view_matchers.withItemCount
import de.troido.reza.domain.random_image.models.RandomImage
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RandomImageActivityTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(RandomImageActivity::class.java)

    @get:Rule
    val idlingResourceRule = EspressoIdlingResourceRule()

    private val context = InstrumentationRegistry.getInstrumentation().targetContext

    private val fakeList = mutableListOf<RandomImage>()
    private val testAdapter = RandomImageAdapter()

    @Before
    fun setUp() {
        fakeList.add(RandomImage("https://images.dog.ceo/breeds/spaniel-welsh/n02102177_2405.jpg"))
        fakeList.add(RandomImage("https://images.dog.ceo/breeds/spaniel-welsh/n02102177_2405.jpg"))
        fakeList.add(RandomImage("https://images.dog.ceo/breeds/spaniel-welsh/n02102177_2405.jpg"))
        fakeList.add(RandomImage("https://images.dog.ceo/breeds/spaniel-welsh/n02102177_2405.jpg"))
        fakeList.add(RandomImage("https://images.dog.ceo/breeds/spaniel-welsh/n02102177_2405.jpg"))
        fakeList.add(RandomImage("https://images.dog.ceo/breeds/spaniel-welsh/n02102177_2405.jpg"))
        fakeList.add(RandomImage("https://images.dog.ceo/breeds/spaniel-welsh/n02102177_2405.jpg"))
        fakeList.add(RandomImage("https://images.dog.ceo/breeds/spaniel-welsh/n02102177_2405.jpg"))
        fakeList.add(RandomImage("https://images.dog.ceo/breeds/spaniel-welsh/n02102177_2405.jpg"))
        fakeList.add(RandomImage("https://images.dog.ceo/breeds/spaniel-welsh/n02102177_2405.jpg"))
        testAdapter.setList(fakeList)
    }

    @After
    fun tearDown() {
        activityRule.scenario.close()
    }

    @Test
    fun testVisibility_startButton() {
        onView(withId(R.id.btnStart))
            .check(matches(isDisplayed()))
    }

    @Test
    fun testVisibility_restartButton() {
        onView(withId(R.id.btnStop))
            .check(matches(isDisplayed()))
    }

    @Test
    fun testVisibility_imageRecyclerview() {
        onView(withId(R.id.rvRandomImages))
            .check(matches(isDisplayed()))
    }

    @Test
    fun testTitle_startAction() {
        onView(isAssignableFrom(Toolbar::class.java))
            .check(
                matches(
                    withActionMenuTitle(
                        context.getString(R.string.activity_random_image_action_start_title)
                    )
                )
            )
    }

    @Test
    fun testTitle_restartAction() {
        onView(isAssignableFrom(Toolbar::class.java))
            .check(
                matches(
                    withActionMenuTitle(
                        context.getString(R.string.activity_random_image_action_restart_title)
                    )
                )
            )
    }

    @Test
    fun testTitle_nextAction() {
        onView(isAssignableFrom(Toolbar::class.java))
            .check(
                matches(
                    withActionMenuTitle(
                        context.getString(R.string.activity_random_image_action_next_title)
                    )
                )
            )
    }

    @Test
    fun testClick_nextAction() {
        onView(withId(R.id.action_next))
            .perform(click())
        //onView(isRoot()).check(matches(withId(R.id.root_testActivity)))
        onView(withId(R.id.root_testActivity)).check(matches(isDisplayed()))
    }

    @Test
    fun testClick_restartAction() {
        openActionBarOverflowOrOptionsMenu(context)
        onView(withText(context.getString(R.string.activity_random_image_action_restart_title)))
            .perform(click())
        onView(withId(R.id.rvRandomImages)).check(matches(withItemCount(0)))
    }

    @Test
    fun testClick_startRestartAction() {
        runBlocking {
            openActionBarOverflowOrOptionsMenu(context)
            onView(withText(context.getString(R.string.activity_random_image_action_start_title))).perform(
                click()
            )
            onView(withId(R.id.rvRandomImages)).check(matches(withItemCount(10)))
            openActionBarOverflowOrOptionsMenu(context)
            onView(withText(context.getString(R.string.activity_random_image_action_restart_title)))
                .perform(click())
            onView(withId(R.id.rvRandomImages)).check(matches(withItemCount(1)))
        }
    }

    @Test
    fun testTitle_startButton() {
        onView(withId(R.id.btnStart)).check(matches(withText("Start")))
    }

    @Test
    fun testTitle_restartButton() {
        onView(withId(R.id.btnStop)).check(matches(withText("Restart")))
    }

    @Test
    fun testVisibility_nextButton() {
        onView(withId(R.id.btn_goToNextActivity)).check(matches(isDisplayed()))
    }

    @Test
    fun testAction_goToNextActivityAndBack() {
        onView(withId(R.id.root_randomImageActivity)).check(matches(isDisplayed()))
        onView(withId(R.id.btn_goToNextActivity)).perform(click())
        onView(withId(R.id.root_testActivity)).check(matches(isDisplayed()))
        pressBack()
        onView(withId(R.id.root_randomImageActivity)).check(matches(isDisplayed()))
    }

    /* @Test
     fun testContent_imageRecyclerView_portrait() {
         activityRule.scenario.onActivity {
             it.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
         }
         runBlocking {
             delay(2000)
             Truth.assertThat(context.resources.configuration.orientation)
                 .isEqualTo(Configuration.ORIENTATION_PORTRAIT)
             //onView(withId(R.id.btnStart)).perform(click())
             openActionBarOverflowOrOptionsMenu(context)
             onView(withText(context.getString(R.string.activity_random_image_action_start_title))).perform(
                 click()
             )
             onView(withId(R.id.rvRandomImages)).check(matches(withItemCount(10)))
             onView(withId(R.id.rvRandomImages)).check(matches(withGridLayoutColumnCount(2)))
         }
     }*/

    /*@Test
    fun testContent_imageRecyclerView_landscape() {
        activityRule.scenario.onActivity {
            it.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        }
        runBlocking {
            delay(2000)
            Truth.assertThat(context.resources.configuration.orientation)
                .isEqualTo(Configuration.ORIENTATION_LANDSCAPE)
            //onView(withId(R.id.btnStart)).perform(click())
            openActionBarOverflowOrOptionsMenu(context)
            onView(withText(context.getString(R.string.activity_random_image_action_start_title))).perform(
                click()
            )
            onView(withId(R.id.rvRandomImages)).check(matches(withItemCount(10)))
            onView(withId(R.id.rvRandomImages)).check(matches(withGridLayoutColumnCount(3)))
        }
    }*/

    @Test
    fun testContent_imageRecyclerView_portrait_() {
        activityRule.scenario.onActivity {
            it.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }

        activityRule.scenario.onActivity {
            val recyclerView = it.findViewById<RecyclerView>(R.id.rvRandomImages)
            recyclerView.adapter = testAdapter
        }
        onView(withId(R.id.rvRandomImages)).check(matches(withItemCount(10)))
        onView(withId(R.id.rvRandomImages)).check(matches(withGridLayoutColumnCount(2)))
    }

    @Test
    fun testContent_imageRecyclerView_landscape_() {
        activityRule.scenario.onActivity {
            it.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        }

        activityRule.scenario.onActivity {
            val recyclerView = it.findViewById<RecyclerView>(R.id.rvRandomImages)
            recyclerView.adapter = testAdapter
        }

        onView(withId(R.id.rvRandomImages)).check(matches(withItemCount(10)))
        onView(withId(R.id.rvRandomImages)).check(matches(withGridLayoutColumnCount(3)))
    }
}