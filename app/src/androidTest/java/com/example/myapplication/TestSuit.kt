package com.example.myapplication

import com.example.myapplication.random_image.RandomImageActivityTest
import com.example.myapplication.test_page.TestActivityTest
import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
    RandomImageActivityTest::class,
    TestActivityTest::class
)
class TestSuit