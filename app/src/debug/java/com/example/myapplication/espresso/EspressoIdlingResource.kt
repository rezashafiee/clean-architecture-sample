package com.example.myapplication.espresso

import androidx.test.espresso.idling.CountingIdlingResource

object EspressoIdlingResource {

    private const val RESOURCE = "GLOBAL"

    @JvmStatic val countingIdlingResource = CountingIdlingResource(RESOURCE)

    fun increment() {
        countingIdlingResource.increment()
    }

    fun decrement() {
        // if counting idling resource is not zero
        if (!countingIdlingResource.isIdleNow)
            countingIdlingResource.decrement()
    }

}