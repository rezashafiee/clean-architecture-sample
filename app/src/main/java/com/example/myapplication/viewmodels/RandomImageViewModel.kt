package com.example.myapplication.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.data.mappers.RandomImageMapper
import com.example.data.network.RandomImageApi
import com.example.data.network.RetrofitFactory
import com.example.data.repository.RandomImageRepositoryImpl
import de.troido.reza.domain.random_image.interactors.GetRandomImageUseCase
import de.troido.reza.domain.random_image.models.RandomImage
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch

class RandomImageViewModel : ViewModel() {

    private val getRandomImageUseCase: GetRandomImageUseCase
    private var gettingRandomImageJob: Job? = null
    private val timer = flow {
        val endingValue = System.currentTimeMillis()
        var currentValue = 0
        emit(currentValue)
        while (currentValue < endingValue) {
            delay(1000)
            emit(currentValue)
            currentValue++
        }
    }

    private val imageList = mutableListOf<RandomImage>()
    private val _randomImageList: MutableLiveData<MutableList<RandomImage>> = MutableLiveData()
    val randomImageList: LiveData<MutableList<RandomImage>> = _randomImageList

    private val _error: MutableLiveData<String> = MutableLiveData()
    val error: LiveData<String> = _error

    init {
        val randomImageApi = RetrofitFactory.create().create(RandomImageApi::class.java)
        val randomImageRepository = RandomImageRepositoryImpl(randomImageApi, RandomImageMapper())
        getRandomImageUseCase = GetRandomImageUseCase(randomImageRepository)
    }

    private fun updateList(randomImage: RandomImage) {
        imageList.add(0, randomImage)
        _randomImageList.postValue(imageList)
    }

    private fun clearList() {
        imageList.clear()
        _randomImageList.postValue(imageList)
    }

    private fun getRandomImage() {
        viewModelScope.launch {
            val randomImageResult = async { getRandomImageUseCase() }
            val result = randomImageResult.await()
            /*result.onSuccess {
                updateList(it)
            }

            result.onFailure {
                _error.postValue(it.message)
            }*/
            updateList(result)
        }
    }

    fun startGettingRandomImage() {
        gettingRandomImageJob = viewModelScope.launch {
            timer.collect {
                getRandomImage()
            }
        }
    }

    fun restartGettingRandomImage() {
        if (gettingRandomImageJob != null)
            gettingRandomImageJob?.cancel()
        clearList()
        startGettingRandomImage()
    }
}