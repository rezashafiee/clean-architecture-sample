package com.example.myapplication.random_image

import android.content.Intent
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.adapters.RandomImageAdapter
import com.example.myapplication.databinding.ActivityRandomImagesBinding
import com.example.myapplication.espresso.EspressoIdlingResource
import com.example.myapplication.test_page.TestActivity
import com.example.myapplication.viewmodels.RandomImageViewModel

class RandomImageActivity : AppCompatActivity() {

    companion object {
        private val TAG: String? = RandomImageActivity::class.java.name
    }

    private val randomImageViewModel: RandomImageViewModel by viewModels()
    private lateinit var viewBinding: ActivityRandomImagesBinding

    private lateinit var randomImageAdapter: RandomImageAdapter
    private lateinit var gridLayoutManager: GridLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityRandomImagesBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        setSupportActionBar(viewBinding.myToolbar)

        subscribeObservers()
        initHandlers()

        randomImageAdapter = RandomImageAdapter()
        randomImageAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)
                if (positionStart == 0 && positionStart == gridLayoutManager.findFirstCompletelyVisibleItemPosition())
                    gridLayoutManager.scrollToPosition(0)
            }
        })

        viewBinding.rvRandomImages.apply {
            val columnNumber = getColumnNumberOnOrientation(resources)
            gridLayoutManager =
                GridLayoutManager(context, columnNumber, GridLayoutManager.VERTICAL, false)
            layoutManager = gridLayoutManager
            adapter = randomImageAdapter
        }
    }

    private fun getColumnNumberOnOrientation(resources: Resources): Int {
        return when (resources.configuration.orientation) {
            Configuration.ORIENTATION_LANDSCAPE -> 3
            else -> 2
        }
    }

    private fun initHandlers() {
        viewBinding.btnStart.setOnClickListener {
            EspressoIdlingResource.increment()
            randomImageViewModel.startGettingRandomImage()
        }

        viewBinding.btnStop.setOnClickListener {
            randomImageViewModel.restartGettingRandomImage()
        }

        viewBinding.btnGoToNextActivity.setOnClickListener {
            val intent = Intent(this, TestActivity::class.java)
            startActivity(intent)
        }
    }

    private fun subscribeObservers() {
        randomImageViewModel.randomImageList.observe(this) { imageList ->
            if (imageList.isEmpty())
                randomImageAdapter.clearList()
            else if (randomImageAdapter.itemCount == 0)
                randomImageAdapter.setList(imageList)
            else
                randomImageAdapter.addImage(imageList[0])
        }

        randomImageViewModel.error.observe(this) {
            Toast.makeText(baseContext, it, Toast.LENGTH_SHORT).show()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_random_image, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.action_start -> {
                EspressoIdlingResource.increment()
                randomImageViewModel.startGettingRandomImage()
                true
            }
            R.id.action_restart -> {
                randomImageViewModel.restartGettingRandomImage()
                true
            }
            else -> {
                val intent = Intent(this, TestActivity::class.java)
                startActivity(intent)
                true
            }
        }
}