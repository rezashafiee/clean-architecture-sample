package com.example.myapplication.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.myapplication.R
import com.example.myapplication.espresso.EspressoIdlingResource
import de.troido.reza.domain.random_image.models.RandomImage

class RandomImageAdapter : RecyclerView.Adapter<RandomImageAdapter.ViewHolder>() {

    private val imageList = mutableListOf<RandomImage>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        LayoutInflater.from(parent.context).apply {
            return ViewHolder(inflate(R.layout.item_random_image, parent, false))
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(imageList[position])
    }

    override fun getItemCount() = imageList.size

    fun addImage(randomImage: RandomImage) {
        imageList.add(0, randomImage)
        notifyItemInserted(0)
        if (imageList.size == 10)
            EspressoIdlingResource.decrement()
    }

    fun clearList() {
        imageList.clear()
        notifyDataSetChanged()
    }

    fun setList(list: List<RandomImage>) {
        imageList.addAll(list)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val ivRandomImage: ImageView = itemView.findViewById(R.id.iv_randomImage)

        fun bind(randomImage: RandomImage) {
            Glide.with(itemView)
                .load(randomImage.imageUrl)
                .into(ivRandomImage)
        }
    }

}