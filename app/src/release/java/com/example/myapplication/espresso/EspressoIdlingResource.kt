package com.example.myapplication.espresso

object EspressoIdlingResource {
    fun increment() {}

    fun decrement() {}
}