package de.troido.reza.domain.random_image.models

data class RandomImage(
    val imageUrl: String?
)