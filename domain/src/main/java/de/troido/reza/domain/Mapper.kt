package de.troido.reza.domain

interface Mapper<FROM, TO> {
    fun map(from: FROM): TO
}