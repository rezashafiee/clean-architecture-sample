package de.troido.reza.domain.random_image.interactors

import de.troido.reza.domain.random_image.repository.RandomImageRepository

class GetRandomImageUseCase(
    private val randomImageRepository: RandomImageRepository
) {

    suspend operator fun invoke() = randomImageRepository.getARandomImage()
}