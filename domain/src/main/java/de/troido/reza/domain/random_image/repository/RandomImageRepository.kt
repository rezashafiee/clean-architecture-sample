package de.troido.reza.domain.random_image.repository

import de.troido.reza.domain.random_image.models.RandomImage

interface RandomImageRepository {
    suspend fun getARandomImage(): RandomImage
}