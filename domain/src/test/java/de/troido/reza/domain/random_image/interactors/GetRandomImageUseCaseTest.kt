package de.troido.reza.domain.random_image.interactors

import com.google.common.truth.Truth
import de.troido.reza.domain.random_image.models.RandomImage
import de.troido.reza.domain.random_image.repository.RandomImageRepository
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.internal.verification.VerificationModeFactory

class GetRandomImageUseCaseTest {

    /*@get:Rule
    val mockkRule = MockKRule(this)

    private val randomImageRepository: RandomImageRepository = mockk()

    @Before
    fun setUp() {
        //randomImageRepository = mock(RandomImageRepository::class.java)

    }

    @Test
    fun `test the tests`() = runBlocking {
        val expected = RANDOM_IMAGE_STUB
        coEvery { randomImageRepository.getARandomImage() } returns Result.success(
            RANDOM_IMAGE_STUB
        )

        val actual = randomImageRepository.getARandomImage()

        Truth.assertThat(actual.getOrNull()).isEqualTo(expected)
        Truth.assertThat(actual.isSuccess).isTrue()
        Truth.assertThat(actual).isInstanceOf(Result::class.java)
        coVerify(exactly = 1) { randomImageRepository.getARandomImage() }
    }

    @Test
    fun `getRandomImage() should returns a success Result object`() = runBlocking {
        val expected = RANDOM_IMAGE_STUB
        `when`(randomImageRepository.getARandomImage()).thenReturn(
            Result.success(
                RANDOM_IMAGE_STUB
            )
        )

        val actual = randomImageRepository.getARandomImage()

        Truth.assertThat(actual.getOrNull()).isEqualTo(expected)
        Truth.assertThat(actual.isSuccess).isTrue()
        Truth.assertThat(actual).isInstanceOf(Result::class.java)
        verify(randomImageRepository, atMost(1)).getARandomImage()
    }

    @Test
    fun `getRandomImage() should returns a failure Result object`() = runBlocking {
        `when`(randomImageRepository.getARandomImage())
            .thenReturn(Result.failure(Exception("")))

        val actual = randomImageRepository.getARandomImage()

        Truth.assertThat(actual.getOrNull()).isInstanceOf(Exception::class.java)
        Truth.assertThat(actual).isInstanceOf(Result::class.java)
        Truth.assertThat(actual.isSuccess).isFalse()
        verify(randomImageRepository, atMost(1)).getARandomImage()
    }

    companion object {
        private val RANDOM_IMAGE_STUB = RandomImage("test_url")
    }*/

    private lateinit var randomImageRepository: RandomImageRepository

    @Before
    fun setUp() {
        randomImageRepository = mock(RandomImageRepository::class.java)
    }

    @Test
    fun `getRandomImage() should return a Result type`() = runBlocking {
        Truth.assertThat(randomImageRepository.getARandomImage()).isInstanceOf(Result::class.java)
    }

    @Test
    fun `getRandomImage() should return a RandomImage when success`(): Unit = runBlocking {
        val expected = Result.success(RandomImage(""))
        `when`(randomImageRepository.getARandomImage()).thenReturn(expected)
        val actual = randomImageRepository.getARandomImage()
        Truth.assertThat(actual).isEqualTo(expected)
        verify(
            randomImageRepository, VerificationModeFactory.description(
                atMostOnce(), "There is a problem for verify method"
            )
        ).getARandomImage()
    }

    @Test
    fun `getRandomImage() should return an exception when failure`(): Unit = runBlocking {
        val expected = Result.failure<Throwable>(Exception(""))
        `when`(randomImageRepository.getARandomImage()).thenReturn(expected)
        val actual = randomImageRepository.getARandomImage()
        Truth.assertThat(actual).isEqualTo(expected)
        verify(
            randomImageRepository, VerificationModeFactory.description(
                atMostOnce(), "There is a problem for verify method"
            )
        ).getARandomImage()
    }

}